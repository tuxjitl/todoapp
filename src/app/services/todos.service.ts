import { Injectable } from '@angular/core';
import { FirebaseStorage } from 'angularfire2';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  todosCollection: AngularFirestoreCollection<Todo>;
  todoDoc: AngularFirestoreDocument<Todo>;
  todos: Observable<Todo[]>;

  constructor(private fs: AngularFirestore) {
    this.todosCollection = this.fs.collection('todos');

    this.todos = this.todosCollection.snapshotChanges().pipe(
      map((changes) => {
        return changes.map((document) => {
          const data = document.payload.doc.data() as Todo;
          data.id = document.payload.doc.id;
          return data;
        });
      })
    );
  }

  //Get all todos from server
  getTodos() {
    return this.todos;
  }

  addTodo(todo:Todo) {
    this.todosCollection.add(todo);
  }

  deleteTodo(todo:Todo){
    this.todoDoc = this.fs.doc(`todos/${todo.id}`);
    this.todoDoc.delete();
  }

  editTodo(todo:Todo){
    this.todoDoc = this.fs.doc(`todos/${todo.id}`);
    this.todoDoc.update(todo);
  }
}
