// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDXt1lqKhPd8CGqlrHfy8WUucHiCJkeEFQ',
    authDomain: 'mytodoapp-11d33.firebaseapp.com',
    projectId: 'mytodoapp-11d33',
    storageBucket: 'mytodoapp-11d33.appspot.com',
    messagingSenderId: '88409771117',
    appId: '1:88409771117:web:6fba1a4f1fd20df181c364',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
